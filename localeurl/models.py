import django.urls

try:
    # Django 2.x
    from django.urls import resolve as django_resolve
    from django.urls import reverse as django_reverse
except ImportError:
    # Django 1.11
    from django.core.urlresolvers import resolve as django_resolve
    from django.core.urlresolvers import reverse as django_reverse
try:
    # Django 1.11
    from django.core import urlresolvers
except ImportError:
    urlresolvers = None

from django.conf import settings
from django.utils import translation

from localeurl import utils


def reverse(*args, **kwargs):
    reverse_kwargs = kwargs.get("kwargs") or {}
    locale = utils.supported_language(
        reverse_kwargs.pop("locale", translation.get_language())
    )
    url = django_reverse_original(*args, **kwargs)
    _, path = utils.strip_script_prefix(url)
    return utils.locale_url(path, locale)


def resolve(*args, **kwargs):
    locale = utils.supported_language(translation.get_language())
    path = args[0]
    if path.startswith("/%s/" % locale):
        path = path[len("/%s" % locale) :]
    return django_resolve_original(path, *args[1:], **kwargs)


django_resolve_original = None
django_reverse_original = None


def patch_reverse():
    """
    Monkey-patches the reverse function to support locale-specific URLs.
    """
    global django_reverse_original
    if django_reverse_original is None:
        django_reverse_original = django_reverse
        django.urls.reverse = reverse
        # Django 1.11
        if urlresolvers:
            urlresolvers.reverse = reverse


def patch_resolve():
    """
    Monkey-patches the resolve function to handle locale-specific URLs.
    """
    global django_resolve_original
    if django_resolve_original is None:
        django_resolve_original = django_resolve
        django.urls.resolve = resolve
        # Django 1.11
        if urlresolvers:
            urlresolvers.resolve = resolve


if settings.USE_I18N:
    patch_reverse()
    patch_resolve()
