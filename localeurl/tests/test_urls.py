"""
URLconf for testing.
"""
try:
    from django.conf.urls.defaults import patterns, url
except ImportError:
    from django.conf.urls import url
    try:
        from django.conf.urls import patterns
    except ImportError:
        def patterns(prefix, *urls):
            return urls


def dummy(request, test="test"):
    pass


urlpatterns = patterns(
    "localeurl.tests.test_urls",
    url(r"^dummy/$", dummy, name="dummy0"),
    url(r"^dummy/(?P<test>.+)$", dummy, name="dummy1"),
)
